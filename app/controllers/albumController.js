const mongoose = require("mongoose");

const albumModel = require("../models/albumModel");

//Create an album
const createAlbum = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newAlbumInput = {
        _id: mongoose.Types.ObjectId(),
        userId: mongoose.Types.ObjectId(),
        title: requestBody.title
    }
    console.log(newAlbumInput)
    albumModel.create(newAlbumInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Album Success",
                data: data
            })
        }
    })
}

//Get all album
const getAllAlbum = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let userId = request.query.userId;
    if (userId) {
        condition.userId = userId
    }
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    albumModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Album",
                data: data
            })
        }
    })
}

//Get photos of album
const getPhotosOfAlbum = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let albumId = request.params.albumId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[albumId] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    albumModel.findById(albumId)
        .populate("photos")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.photos
                })
            }
        })
    }

//Get an album by id
const getAlbumById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let albumId = request.params.albumId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        response.status(400).json({
            message: "[albumId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        albumModel.findById(albumId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get Album By Id",
                    data: data
                })
            }
        })
    }
}

//Update album by id
const updateAlbumById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let albumId = request.params.albumId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        response.status(400).json({
            message: "[albumId] is invalid!"
        })
    }
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updateAlbumInput = {
        title: requestBody.title
    }
    console.log(updateAlbumInput);
    albumModel.findByIdAndUpdate(albumId, updateAlbumInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Album By Id",
                data: data
            })
        }
    })
}

//Delete album by id
const deleteAlbumById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let albumId = request.params.albumId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[albumId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        albumModel.findByIdAndDelete(albumId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Album Success",
                    data: data
                })
            }
        })
    }
}

//Export module
module.exports = { createAlbum, getAllAlbum, getAlbumById, getPhotosOfAlbum, updateAlbumById, deleteAlbumById }