const mongoose = require("mongoose");

const commentModel = require("../models/commentModel");

//Create a comment
const createComment = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newCommentInput = {
        _id: mongoose.Types.ObjectId(),
        postId: mongoose.Types.ObjectId(),
        name: requestBody.name,
        email: requestBody.email
    }
    console.log(newCommentInput)
    commentModel.create(newCommentInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Comment Success",
                data: data
            })
        }
    })
}
//Get all comment
const getAllComment = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let postId = request.query.postId;
    if (postId) {
        condition.postId = postId
    }
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    commentModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Post",
                data: data
            })
        }
    })
}

//Get a comment by id
const getCommentById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let commentId = request.params.commentId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        response.status(400).json({
            message: "[commentId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        commentModel.findById(commentId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get A Comment By Id",
                    data: data
                })
            }
        })
    }
}
//Update a comment by id
const updateCommentById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let commentId = request.params.commentId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        response.status(400).json({
            message: "[commentId] is invalid!"
        })
    }
    if (!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updateCommentInput = {
        name: requestBody.name,
        emai: requestBody.email
    }
    console.log(updateCommentInput)
    commentModel.findByIdAndUpdate(commentId, updateCommentInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Comment By Id",
                data: data
            })
        }
    })
}
//Delete a comment by id
const deleteCommentById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let commentId = request.params.commentId;
    
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[commentId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        commentModel.findByIdAndDelete(commentId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Comment Success",
                    data: data
                })
            }
        })
    }
}
//Export module
module.exports = { createComment, getAllComment, getCommentById, updateCommentById, deleteCommentById }