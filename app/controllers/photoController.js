const mongoose = require("mongoose");

const photoModel = require("../models/photoModel");

//Create a photo
const createPhoto = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newPhotoInput = {
        _id: mongoose.Types.ObjectId(),
        userId: mongoose.Types.ObjectId(),
        title: requestBody.title
    }
    console.log(newPhotoInput)
    photoModel.create(newPhotoInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Photo Success",
                data: data
            })
        }
    })
}
//Get all photos
const getAllPhoto = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let albumId = request.query.albumId;
    if(albumId){
        condition.albumId = albumId
    }
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    photoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Photo",
                data: data
            })
        }
    })
}
//Get a photo by id
const getPhotoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let photoId = request.params.photoId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        response.status(400).json({
            message: "[photoId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        photoModel.findById(photoId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get Photo By Id",
                    data: data
                })
            }
        })
    }
}
//Update a photo by id
const updatePhotoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let photoId = request.params.photoId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        response.status(400).json({
            message: "[photoId] is invalid!"
        })
    }
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updatePhotoInput = {
        title: requestBody.title
    }
    console.log(updatePhotoInput);
    photoModel.findByIdAndUpdate(photoId, updatePhotoInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Photo By Id",
                data: data
            })
        }
    })
}

//Delete a photo by id
const deletePhotoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let photoId = request.params.photoId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[photoId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        photoModel.findByIdAndDelete(photoId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Photo Success",
                    data: data
                })
            }
        })
    }
}
//Export module
module.exports = { createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById }