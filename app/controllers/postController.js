const mongoose = require("mongoose");

const postModel = require("../models/postModel");
const userModel = require("../models/userModel");

//Create a post
const createPost = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    if (!requestBody.body) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[body] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newPostInput = {
        _id: mongoose.Types.ObjectId(),
        userId: mongoose.Types.ObjectId(),
        title: requestBody.title,
        body: requestBody.body
    }
    console.log(newPostInput)
    postModel.create(newPostInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Post Success",
                data: data
            })
        }
    })
}
//Get all post 
const getAllPost = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let userId = request.query.userId;
    if (userId) {
        condition.userId = userId
    }
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    postModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Post",
                data: data
            })
        }
    })
}
//Get a comment of post
const getCommentOfPost = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let postId = request.params.postId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[postId] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    postModel.findById(postId)
        .populate("comments")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.comments
                })
            }
        })
    }
//Get a post by id
const getAllPostById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let postId = request.params.postId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            message: "[postId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        postModel.findById(postId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Post By Id",
                    data: data
                })
            }
        })
    }
}
//Update a post by id
const updatePostById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let postId = request.params.postId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            message: "[postId] is invalid!"
        })
    }
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    if (!requestBody.body) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[body] is required!"
        })
    }

    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updatePostInput = {
        title: requestBody.title,
        body: requestBody.body
    }
    console.log(updatePostInput);
    postModel.findByIdAndUpdate(postId, updatePostInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Post By Id",
                data: data
            })
        }
    })
}

//Delete a post by id
const deletePostById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let postId = request.params.postId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        postModel.findByIdAndDelete(postId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Post Success",
                    data: data
                })
            }
        })
    }
}

//Export module
module.exports = { createPost, getAllPost, getCommentOfPost, getAllPostById, updatePostById, deletePostById }