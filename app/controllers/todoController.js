const mongoose = require("mongoose");

const todoModel = require("../models/todoModel");

//Create a Todo
const createTodo = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newTodoInput = {
        _id: mongoose.Types.ObjectId(),
        userId: mongoose.Types.ObjectId(),
        title: requestBody.title
    }
    console.log(newTodoInput)
    todoModel.create(newTodoInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create Todo Success",
                data: data
            })
        }
    })
}
//Get all Todo
const getAllTodo = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let condition = {};
    let userId = request.query.userId;
    if(userId){
        condition.userId = userId
    }
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    todoModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All Todo",
                data: data
            })
        }
    })
}

//Get a Todo By Id
const getTodoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let todoId = request.params.todoId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        response.status(400).json({
            message: "[todoId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        todoModel.findById(todoId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get Todo By Id",
                    data: data
                })
            }
        })
    }

}

//Update a Todo By Id
const updateTodoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let todoId = request.params.todoId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        response.status(400).json({
            message: "[todoId] is invalid!"
        })
    }
    if (!requestBody.title) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[title] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updateTodoInput = {
        title: requestBody.title
    }
    console.log(updateTodoInput);
    todoModel.findByIdAndUpdate(todoId, updateTodoInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Todo By Id",
                data: data
            })
        }
    })
}

//Delete a Todo By Id
const deleteTodoById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let todoId = request.params.todoId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[todoId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        todoModel.findByIdAndDelete(todoId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete Todo Success",
                    data: data
                })
            }
        })
    }
}

//Export module
module.exports = { createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById }