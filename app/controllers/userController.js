const mongoose = require("mongoose");
const postModel = require("../models/postModel");

const userModel = require("../models/userModel");

//Create a user
const createUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    }
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[username] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    if (!requestBody.address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[address] is required!"
        })
    }
    if (!requestBody.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[phone] is required!"
        })
    }
    if (!requestBody.website) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[website] is required!"
        })
    }
    if (!requestBody.company) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[company] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let newUserInput = {
        _id: mongoose.Types.ObjectId(),
        name: requestBody.name,
        username: requestBody.username,
        email: requestBody.email,
        address: requestBody.address,
        phone: requestBody.phone,
        website: requestBody.website,
        company: requestBody.company
    }
    console.log(newUserInput)
    userModel.create(newUserInput, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Create User Success",
                data: data
            })
        }
    })
}

//Get all users
const getAllUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu (Bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (Bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get All User",
                data: data
            })
        }
    })
}

//Get a post of user
const getPostsOfUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.findById(userId)
        .populate("posts")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.posts
                })
            }
        })
}

//Get an album of user
const getAlbumOfUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    }

    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.findById(userId)
        .populate("albums")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.albums
                })
            }
        })
}

//Get a Todo of user
const getTodosOfUser = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    }
    //Bước 3: Thực hiện thao tác dữ liệu
    userModel.findById(userId)
        .populate("todos")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get Data Success",
                    data: data.todos
                })
            }
        })
    }

//Get a user by id
const getAllUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        userModel.findById(userId, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All User",
                    data: data
                })
            }
        })
    }
}

//Update a user by id
const updateUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;
    let requestBody = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            message: "[userId] is invalid!"
        })
    }
    if (!requestBody.name) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[name] is required!"
        })
    }
    if (!requestBody.username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[username] is required!"
        })
    }
    if (!requestBody.email) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[email] is required!"
        })
    }
    if (!requestBody.address) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[address] is required!"
        })
    }
    if (!requestBody.phone) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[phone] is required!"
        })
    }
    if (!requestBody.website) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[website] is required!"
        })
    }
    if (!requestBody.company) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "[company] is required!"
        })
    }
    //Bước 3: Thực hiện các thao tác nghiệp vụ
    let updateUserInput = {
        name: requestBody.name,
        username: requestBody.username,
        email: requestBody.email,
        address: requestBody.address,
        phone: requestBody.phone,
        website: requestBody.website,
        company: requestBody.company
    }
    console.log(updateUserInput);
    userModel.findByIdAndUpdate(userId, updateUserInput, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update User By Id",
                data: data
            })
        }
    })
}

//Delete a user by id
const deteleUserById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let userId = request.params.userId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "[userId] is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        userModel.findByIdAndDelete(userId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                response.status(204).json({
                    status: "Success: Delete User Success",
                    data: data
                })
            }
        })
    }
}

//Export module
module.exports = { createUser, getAllUser, getAllUserById, getPostsOfUser, getAlbumOfUser, getTodosOfUser, updateUserById, deteleUserById }