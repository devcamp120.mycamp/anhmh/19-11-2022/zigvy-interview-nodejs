//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const commentSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    postId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
})

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Comment", commentSchema);