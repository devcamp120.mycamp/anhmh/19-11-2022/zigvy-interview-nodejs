//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const photoSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    albumId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    title: {
        type: String,
        unique: true,
        required: true
    },
    url: {
        type: String,
        required: false
    },
    thumbnailUrl: {
        type: String,
        required: false
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
})
//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Photo", photoSchema);

/*
{
    "albumId": 1,
    "id": 1,
    "title": "accusamus beatae ad facilis cum similique qui sunt",
    "url": "https://via.placeholder.com/600/92c952",
    "thumbnailUrl": "https://via.placeholder.com/150/92c952"
  }
*/