//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const postSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    userId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    title: {
        type: String,
        unique: true,
        required: true
    },
    body: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    },
    comments: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Comment"
        }
    ]
})

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Post", postSchema);