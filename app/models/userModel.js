//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const userSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    address: {
        type: String,
        unique: true,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    website: {
        type: String,
        required: true
    },
    company: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    },
    posts: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Post"
        }
    ],
    albums: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Album"
        }
    ],
    todos: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Todo"
        }
    ]
})

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("User", userSchema);