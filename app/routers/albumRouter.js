//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createAlbum, getAllAlbum, getAlbumById, getPhotosOfAlbum, updateAlbumById, deleteAlbumById } = require("../controllers/albumController");
const albumRouter = express.Router();

//Create an album API
albumRouter.post("/albums", createAlbum);

//Get all album
albumRouter.get("/albums", getAllAlbum);

//Get an album by id
albumRouter.get("/albums/:albumId", getAlbumById);

//Get photos of album
albumRouter.get("/albums/:albumId/photos", getPhotosOfAlbum);

//Update an album by id
albumRouter.put("/albums/:albumId", updateAlbumById);

//Delete an album by id
albumRouter.delete("/albums/:albumId", deleteAlbumById);

//Export dữ liệu thành 1 model
module.exports = albumRouter;