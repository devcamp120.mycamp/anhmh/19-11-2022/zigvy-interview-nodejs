//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createComment, getAllComment, getCommentById, updateCommentById, deleteCommentById } = require("../controllers/commentController");
const commentRouter = express.Router();

//Create a comment API
commentRouter.post("/comments", createComment);

//Get all comment
commentRouter.get("/comments", getAllComment);

//Get a comment by id
commentRouter.get("/comments/:commentId", getCommentById);

//Update a comment by id
commentRouter.put("/comments/:commentId", updateCommentById);

//Delete a comment by id
commentRouter.delete("/comments/:commentId", deleteCommentById);

//Export dữ liệu thành 1 model
module.exports = commentRouter;
