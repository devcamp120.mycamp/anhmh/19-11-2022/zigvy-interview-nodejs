//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById } = require("../controllers/photoController");
const photoRouter = express.Router();

//Create a photo
photoRouter.post("/photos", createPhoto);

//Get all photos
photoRouter.get("/photos", getAllPhoto);

//Get a photo by id
photoRouter.get("/photos/:photoId", getPhotoById);

//Update a photo by id
photoRouter.put("/photos/:photoId", updatePhotoById);

//Delete a photo by id
photoRouter.delete("/photos/:photoId", deletePhotoById);

//Export dữ liệu thành 1 model
module.exports = photoRouter;