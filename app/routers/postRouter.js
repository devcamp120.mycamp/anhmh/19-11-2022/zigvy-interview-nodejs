//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createPost, getAllPost, getAllPostById, getCommentOfPost, updatePostById, deletePostById } = require("../controllers/postController");
const postRouter = express.Router();

//Creat a post API
postRouter.post("/posts", createPost);

//Get all post API
postRouter.get("/posts", getAllPost);

//Get a post by id API
postRouter.get("/posts/:postId", getAllPostById);

//Get a comment of post
postRouter.get("/posts/:postId/comments", getCommentOfPost);

//Update post by id API
postRouter.put("/posts/:postId", updatePostById);

//Delete post by id API
postRouter.delete("/posts/:postId", deletePostById);

//Export dữ liệu thành 1 model
module.exports = postRouter;