//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById } = require("../controllers/todoController");
const todoRouter = express.Router();


//Create a Todo
todoRouter.post("/todos", createTodo)

//Get all Todo
todoRouter.get("/todos", getAllTodo)

//Get a Todo By Id
todoRouter.get("/todos/:todoId", getTodoById)

//Update a Todo By Id
todoRouter.put("/todos/:todoId", updateTodoById)

//Delete a Todo By Id
todoRouter.delete("/todos/:todoId", deleteTodoById)

//Export dữ liệu thành 1 module
module.exports = todoRouter;