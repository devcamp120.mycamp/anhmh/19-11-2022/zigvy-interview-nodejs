//Import bộ thư viện Express
const express = require('express');

//Import module controllers
const { createUser, getAllUser, getAllUserById, getPostsOfUser, getAlbumOfUser, getTodosOfUser, updateUserById, deteleUserById } = require("../controllers/userController");
const userRouter = express.Router();

//Create a user
userRouter.post("/users", createUser);

//Get a user
userRouter.get("/users", getAllUser);

//Get a user by id
userRouter.get("/users/:userId", getAllUserById);

//Get a post of user
userRouter.get("/users/:userId/posts", getPostsOfUser);

//Get an album of user
userRouter.get("/users/:userId/albums", getAlbumOfUser);

//Get Todo of user
userRouter.get("/users/:userId/todos", getTodosOfUser);

//Put a user by id
userRouter.put("/users/:userId", updateUserById);

//Delete a user by id
userRouter.delete("/users/:userId", deteleUserById);

//Export dữ liệu thành 1 module 
module.exports = userRouter;