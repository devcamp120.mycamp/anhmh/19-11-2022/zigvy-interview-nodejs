//Import express from 'express'
//Dùng để import thư viện vào project
const express = require("express");

//Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Kết nối đến cơ sở dữ liệu Mongoose
mongoose.connect('mongodb://localhost:27017/CRUD_ZigvyProject', (error) => {
    if(error){
        throw error;
    }
    console.log("Successfully connected!");
});

//Khai báo các Models (Import Models)
const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");
const photoModel = require("./app/models/photoModel");
const todoModel = require("./app/models/todoModel");
const albumModel = require("./app/models/albumModel");

//Khai báo các Routers (Import Routers)
const userRouter = require("./app/routers/userRouter");
const postRouter = require("./app/routers/postRouter");
const commentRouter = require("./app/routers/commentRouter");
const albumRouter = require("./app/routers/albumRouter");
const photoRouter = require("./app/routers/photoRouter");
const todoRouter = require("./app/routers/todoRouter");
//Khởi tạo app express
const app = express();

//Khai báo cổng của project
const port = 8000;

//Sử dụng được body json
app.use(express.json());

//Sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//Khai báo thư viện path
const path = require("path");

//Khai báo sử dụng các Routers
app.use("/", userRouter);
app.use("/", postRouter);
app.use("/", commentRouter);
app.use("/", albumRouter);
app.use("/", photoRouter);
app.use("/", todoRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})